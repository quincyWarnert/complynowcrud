<?php
namespace App\Controller;

use App\Entity\Wet;
use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class wetController extends AbstractController {
    /**
     * @Route("/", name="wet_overzicht",methods={"GET"})
     *
     */
public function index(){

    $wetten=$this->getDoctrine()
    ->getRepository(Wet::class)->findAll();

return $this->render('wet/index.html.twig',array('wetten'=>$wetten));
}

 /**
     * @Route("/wet/nieuw", name="nieuw_wet", methods={"GET","POST"})
     * 
     */
    public function nieuw(Request $request) {
        $wet = new Wet();
        $form = $this->createFormBuilder($wet)
          ->add('title', TextType::class, array('attr' => array('class' => 'form-control')))
          ->add('beschrijving', TextareaType::class, array(
            'required' => false,
            'attr' => array('class' => 'form-control')
          ))
          ->add('save', SubmitType::class, array(
            'label' => 'Create',
            'attr' => array('class' => 'btn btn-primary mt-3')
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
          $wet = $form->getData();
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($wet);
          $entityManager->flush();
          return $this->redirectToRoute('wet_overzicht');
        }
        return $this->render('wet/nieuw.html.twig', array(
          'form' => $form->createView()
        ));
      }




/**
     * @Route("/wet/edit/{id}", name="edit_wet", methods={"GET","POST"})
     * 
     */
    public function edit(Request $request, $id) {
        $wet = new Wet();
        $wet = $this->getDoctrine()->getRepository(Wet::class)->find($id);
        $form = $this->createFormBuilder($wet)
          ->add('title', TextType::class, array('attr' => array('class' => 'form-control')))
          ->add('beschrijving', TextareaType::class, array(
            'required' => false,
            'attr' => array('class' => 'form-control')
          ))
          ->add('save', SubmitType::class, array(
            'label' => 'Update',
            'attr' => array('class' => 'btn btn-primary mt-3')
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->flush();
          return $this->redirectToRoute('wet_overzicht');
        }
        return $this->render('wet/edit.html.twig', array(
          'form' => $form->createView()
        ));
      }

/**
 * @Route("/wet/{id}", name="wet_beschrijving")
 */
public function beschrijving($id){
    $eenWet =$this->getDoctrine()
    ->getRepository(Wet::class)->find($id);
return $this->render('wet/beschrijving.html.twig', array('wet'=> $eenWet));

}
     /** 
     * @Route("/wet/delete/{id}", methods={"DELETE"})
     * 
     */
    public function delete(Request $request, $id) {
        $wet = $this->getDoctrine()->getRepository(Wet::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($wet);
        $entityManager->flush();
        $response = new Response();
        $response->send();
      }

}
