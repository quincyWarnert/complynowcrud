<?php

namespace App\Repository;

use App\Entity\Wet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Wet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wet[]    findAll()
 * @method Wet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Wet::class);
    }

    // /**
    //  * @return Wet[] Returns an array of Wet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wet
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
