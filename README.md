Crud
•	Een applicatie waar er een wet en een beschrijving kunnen worden ingevoerd, bijgewerkt, verwijderd en een nieuwe aanmaken.
Getting started
•	Install dependencies
Composer install
•	De .env file bijwerken voor de database
Schema maken
•	Php bin/console doctrine:migrations:diff
Run migration
•	Php bin/console doctrine:migrations:migrate
Build for production
•	Npm run build

