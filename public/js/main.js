const wet = document.getElementById("wet");

if (wet) {
  wet.addEventListener("click", e => {
    if (e.target.className === "btn btn-danger delete-wet") {
      if (confirm("Are you sure?")) {
        const id = e.target.getAttribute("data-id");

        fetch(`/wet/delete/${id}`, {
          method: "DELETE"
        }).then(res => window.location.reload());
      }
    }
  });
}
